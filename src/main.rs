/*
 *  Copyright (C) 2023 marcu5h (https://gitlab.com/Marcu5H)
 *
 *  This file is part of ffind.
 *
 *  ffind is free software: you can redistribute it and/or modify it under the
 *  terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  ffind is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *  for more details.
 *
 *  You should have received a copy of the GNU General Public License along with
 *  ffind. If not, see <https://www.gnu.org/licenses/>.
*/

use clap::Parser;
use rayon::ThreadPoolBuilder;
use regex::Regex;
use colored::Colorize;
#[macro_use]
extern crate lazy_static;
use std::{
    fs,
    path::{Path, PathBuf},
    sync::{Arc, Mutex},
    borrow::Cow, collections::VecDeque, io::{self, Write},
};

// TODO: Make much faster
struct FFLog {
    messages: VecDeque<String>,
    buffer_size: usize,
}

impl FFLog {
    pub fn new() -> Self {
        return Self {
            messages: VecDeque::new(),
            buffer_size: 1,
        }
    }

    pub fn set_buffer_size(&mut self, size: usize) {
        self.buffer_size = size;
    }

    pub fn flush(&mut self) {
        let mut stdout = io::stdout();

        while let Some(message) = self.messages.pop_front() {
            // TODO: Handle error
            let _ = stdout.write(message.as_bytes());
        }

        // TODO: Handle error
        let _ = stdout.flush();
    }

    pub fn log(&mut self, msg: String) {
        self.messages.push_back(msg);

        if self.messages.len() >= self.buffer_size {
            self.flush();
        }
    }
}

lazy_static! {
    static ref GLOBAL_LOGGER: Mutex<FFLog> = Mutex::new(FFLog::new());
}

macro_rules! fflog {
    ($($arg:tt)*) => {
        $crate::GLOBAL_LOGGER.lock().unwrap().log(format!($($arg)*).to_string())
    };
}

macro_rules! fflog_flush {
    () => {
        $crate::GLOBAL_LOGGER.lock().unwrap().flush()
    };
}

macro_rules! fflog_set_cap {
    ($cap:expr) => {
        $crate::GLOBAL_LOGGER.lock().unwrap().set_buffer_size($cap)
    };
}

// The different type of file types ffind can find
#[derive(Clone, PartialEq, Eq)]
enum FindType {
    File,
    Directory,
}

impl std::str::FromStr for FindType {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "f" | "file" => Ok(FindType::File),
            "d" | "dir" | "directory" => Ok(FindType::Directory),
            _ => Err(format!("Invalid value for type: {}", s)),
        }
    }
}

impl std::fmt::Display for FindType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self {
            FindType::File => write!(f, "file"),
            FindType::Directory => write!(f, "directory"),
        }
    }
}

#[derive(Parser)]
#[command(author = "marcu5h (https://gitlab.com/Marcu5H)")]
#[command(version, about, long_about = None)]
struct Args {
    /// Where to start finding
    #[arg(short, long, default_value = ".")]
    dir: PathBuf,

    /// Type to find
    #[arg(short, long, name = "type", default_value_t = FindType::File,
          value_parser = clap::value_parser!(FindType))]
    typ: FindType,

    /// Expression to look with
    #[arg(short, long, default_value ="")]
    name: String,

    /// The amount of threads to use
    #[arg(short = 'T', long, default_value_t = 4)]
    threads: usize,

    /// Whether to ignore error messages or not
    #[arg(short, long, default_value_t = true)]
    ignore_errors: bool,

    /// Wheter to highlight the matches or not
    #[arg(short, long, default_value_t = false)]
    color: bool,

    /// Buffer size, higher number might lead to faster execution
    #[arg(short, long, name = "buffer-capacity", value_name = "CAPACITY",
          default_value_t = 1)]
    buffer_capacity: usize,
}

struct FFind {
    pub exp: Regex,
    pub typ: FindType,
    pub errors: bool,
    pub color: bool,
}

// TODO: Handle errors
#[inline(always)]
fn color_highlight_match<'t>(exp: &Regex, s: &'t str) -> Cow<'t, str> {
    return exp.replace_all(s, |caps: &regex::Captures<'_>| {
        format!("{}", caps.get(0).unwrap().as_str().red())
    });
}

#[inline(always)]
fn if_match_print(exp: &Regex, s: &Path, color: bool) {
    let parent_name = s.parent();
    if parent_name.is_none() {
        return;
    }
    let parent_name = parent_name.unwrap().to_str().unwrap();

    let suf_name = s.file_name();
    if suf_name.is_none() {
        return;
    }
    let suf_name = suf_name.unwrap().to_str().unwrap();

    if exp.is_match(suf_name) {
        if color {
            fflog!("{}/{}\n", parent_name,
                     color_highlight_match(exp, suf_name));
        } else {
            fflog!("{}/{}\n", parent_name, suf_name);
        }
    }
}

fn find(ff: Arc<FFind>, p: PathBuf) {
    match fs::read_dir(&p) {
        Ok(entries) => {
            for entry in entries {
                match entry {
                    Ok(entry) => {
                        if let Ok(metadata) = entry.metadata() {
                            if (ff.typ == FindType::Directory
                                && metadata.is_dir())
                                || (ff.typ == FindType::File
                                && metadata.is_file())
                            {
                                if_match_print(&ff.exp, &entry.path(), ff.color);
                            }
                            if metadata.is_dir() {
                                let ff_clone = Arc::clone(&ff);
                                rayon::spawn(move || {
                                    find(ff_clone, entry.path())
                                });
                            }
                        }
                    }
                    Err(e) => {
                        if ff.errors {
                            eprintln!("{}: {}", p.to_str().unwrap(), e);
                        }
                    }
                }
            }
        }
        Err(e) => {
            if ff.errors {
                eprintln!("{}: {}", p.to_str().unwrap(), e);
            }
        }
    }
}

fn die(msg: &str) {
    eprintln!("{}", msg);
    std::process::exit(1);
}

fn main() {
    let args = Args::parse();

    let find_expression = match Regex::new(&args.name) {
        Ok(e) => e,
        Err(_) => {
            die("Failed to compile search expression");
            panic!("Failed to die"); // Make compiler happy
        }
    };

    let tpool = match ThreadPoolBuilder::new().num_threads(args.threads).build()
    {
        Ok(tp) => tp,
        Err(_) => {
            die("Failed to create threadpool");
            panic!("Failed to die"); // Make compiler happy
        }
    };

    let ff: Arc<FFind> = Arc::new(FFind {
        exp: find_expression,
        typ: args.typ,
        errors: !args.ignore_errors,
        color: args.color,
    });

    fflog_set_cap!(args.buffer_capacity);

    tpool.install(|| find(Arc::clone(&ff), args.dir));

    // Wait for all the threads to finish executing
    tpool.join(|| {}, || {});

    fflog_flush!();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_no_dir() {
        let flags = vec!["ffind"];
        let args = Args::parse_from(flags);
        assert_eq!(args.dir, PathBuf::from("."));
    }

    #[test]
    fn test_no_name() {
        let flags = vec!["ffind"];
        let args = Args::parse_from(flags);
        assert_eq!(args.name, "");
    }

    #[test]
    fn test_10_threads() {
        let flags = vec!["ffind", "-T", "10"];
        let args = Args::parse_from(flags);
        assert_eq!(args.threads, 10);
    }

    #[test]
    fn test_default_threads() {
        let flags = vec!["ffind"];
        let args = Args::parse_from(flags);
        assert_eq!(args.threads, 4);
    }
}
