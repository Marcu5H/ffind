# ffind
Ffind was created because I was tired of waiting for `find` to finish.

## License
All files in this project are licensed under the GPLv3.
Read [LICENSE](./LICENSE) for more.

## Installing
To install ffind clone the git repo:
``` shell
$ git clone https://gitlab.com/Marcu5H/ffind.git
```

Change to the directory:
``` shell
$ cd ffind
```

To build the binary you need to have [Cargo](https://github.com/rust-lang/cargo)
installed on your system.

Run the following command to build and install:

``` shell
$ cargo install --path .
```

## Examples
To search for files with the `.pdf` file extension in you home directory:

``` shell
$ ffind -d ~ --typ file -n "\.pdf$"
```

If you want to use 8 threads and have colored output:

``` shell
$ ffind -d ~ --typ file -n "\.pdf$" --threads 8 --color
```

## Changelog

### 0.1.2
Lines of code: 230

#### Added
- Custom logging
- Ability to change the buffer size of the logger

### 0.1.1
Lines of code: 148

#### Added

- Option for colored output

### 0.1.0
Lines of code: 142

ffind.
